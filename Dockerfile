FROM debian:9 AS builder
WORKDIR /build/
RUN apt-get update && apt-get install -y gcc make wget libpcre3 libpcre3-dev zlib1g-dev
RUN wget -c wget http://nginx.org/download/nginx-1.0.5.tar.gz -O - | tar -xz
RUN cd nginx-1.0.5 && ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=builder /usr/local/nginx/sbin/nginx ./
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod 755 nginx
COPY --from=builder /usr/local/nginx/conf/nginx.conf ../conf
COPY --from=builder /usr/local/nginx/conf/mime.types ../conf
COPY --from=builder /usr/local/nginx/html/index.html ../html
CMD ["./nginx", "-g", "daemon off;"]
